# before we start this, download the latest stage3 by going to
# release/amd64/autobuilds/current-stage3-amd64
# links http://www.gentoo.org/main/en/mirrors.xml

# create partition
export PARTED=parted -a optimal /dev/sda

$PARTED mklabel gpt
$PARTED mkpart primary 1 3
$PARTED name 1 grub
$PARTED set 1 bios_grub on
$PARTED mkpart primary 3 131
$PARTED name 2 boot
$PARTED mkpart primary 131 643
$PARTED name 3 swap
$PARTED mkpart primary 643 -- -1
$PARTED name 4 rootfs

mkfs.ext2 /dev/sda2
mkfs.ext4 /dev/sda4
mkswap /dev/sda3
swapon /dev/sda3

mount /dev/sda4 /mnt/gentoo
mkdir -p /mnt/gentoo/boot
mount /dev/sda2 /mnt/gentoo/boot

mv stage3-*.tar.bz2 /mnt/gentoo
cd /mnt/gentoo
tar xvjpf stage3-*.tar.bz2
