;; -*- eval: (outline-minor-mode); -*-
;; init.el --- The start of a beautiful configuration

;; if you're (me) confused about all the bindings, refer to `describe-personal-keybindings'
;; the configuration is split into multiple files to allow each specific configs to load only what they need

;;; use-package
(require 'package)
(dolist (parchive '(("melpa" . "http://melpa.org/packages/")
		    ("org" . "http://orgmode.org/elpa/")))
  (add-to-list 'package-archives parchive))
(package-initialize)
;; the package `org-mode-contrib' contains the latest and greatest org-mode.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package)
  (package-install 'org-plus-contrib))

(setq use-package-always-ensure t
      use-package-always-defer nil)

;; w/o this, emacs will cry everytime it follows vc-ed files
(setq vc-follow-symlinks t)

;;; the rest
(org-babel-load-file "~/.emacs.d/init.el.org")
