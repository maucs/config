set backspace=indent,eol,start
set hidden " easy buffer switching

set tw=0 wrap linebreak " doesn't break when typing a line too long
set autoindent
set smartindent
set ts=4 sts=4 sw=4 noexpandtab " set tabs
set scrolloff=9 " scrolloff, easier to read

set colorcolumn=80
set relativenumber

set shortmess=at " short message
set showmatch " show matching parenthesis
set fileformats=unix,dos,mac
set ignorecase
set incsearch
set smartcase
set hlsearch

set wildmenu " command line completion
set wildmode=list:full " shows all commands available
set report=0 " always report number of line changed
set showcmd " display incomplete commands
set history=1000

set backup
set backupdir=~/.vim//
set directory=~/.vim//

noremap ; :
noremap : ; " switch ; and :

noremap <esc><esc> :noh<CR> " remove highlight after search

cmap w!! w !sudo tee % >/dev/null " sudo save file. very useful
