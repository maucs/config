#!/usr/bin/env bash

wm="$HOME/monsterwm/monsterwm"
ff="/tmp/monsterwm.fifo"
font="-misc-terminusmod.icons-medium-r-normal--12-120-72-72-c-60-*-*"

[[ -p $ff ]] || mkfifo -m 600 "$ff"

tags=('\o/' '\oo/' '\ooo/' '\oooo/')
layouts=('T' 'M' 'B' 'G' 'F')

# Check if it's a pipe, otherwise create it
[[ -p $ff ]] || mkfifo -m 600 "$ff"

while read -t 60 -r wmout || true; do
    desktops=( $(cut -d"|" -f1 <<< "$wmout") )
    title="$(cut -d"|" -f2- <<< "$wmout")"

    if [[ "${desktops[@]}" =~ ^(([[:digit:]]+:)+[[:digit:]]+ ?)+$ ]]; then
	unset tmp

	for desktop in "${desktops[@]}"; do
	    IFS=':' read -r d w m c u <<< "$desktop"

	    # Tags labels
	    label="${tags[$d]}"

	    # Is this the current desktop ? save the layout
	    ((c)) && fg="#fefefe" bg="#204a87" && layout="${layouts[$m]}" \
		|| fg="#b3b3b3" bg=""

	    # Has windows ?
	    ((w)) && fg="#d11783"

	    # Urgent windows ?
	    ((u)) && fg="#ef2929"

	    tmp+="^fg($fg)^bg($bg) $label ^bg()^fg() :: "
	done
    fi

    # Merge the clients indications, the tile and the info
    #echo "$tmp <$layout> $title"
    echo "$tmp<$layout>"
done < "$ff" | dzen2 -h 18 -ta l -e -p -fn $font &

while :; do "$wm" || break; done | tee -a "$ff"
