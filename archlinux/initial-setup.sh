export PACMAN="pacman -Syu --needed --noconfirm"
export SCRIPT="./scripts"
# documentation pre-install and during install
# info telling user to copy ssh key

# post-install
# run as root
systemctl enable dhcpcd@enp0s3
systemctl start dhcpcd@enp0s3


$PACMAN base-devel
$SCRIPT/vbox.sh

# reduce syslinux boot time to 1.5s
sed -i s/"TIMEOUT 50"/"TIMEOUT 15"/g /boot/syslinux/syslinux.cfg

# make new user, sudo is already part of base-devel
echo "%wheel ALL=(ALL) ALL" > /etc/sudoers.d/g-wheel
useradd -m -g wheel -s /bin/bash mau; passwd mau

$SCRIPT/pacaur.sh

echo -e '\E[0;32m'"Set up ssh keys, and then run ssh.sh, then clone git@bitbucket.org/maucs/mau-config.git"'\E[0m'

# cp -r `` /home/mau/Documents/git

# su to new user and do this
# -------
# Use quicklisp to install SLIME, and run (ql:add-to-init-file)
# Setup weechat, follow readme.rst
# Remember to set up git name and email address
# Run 'fbsetbg ~/.wallpaper' to set wallpaper. Do it once only

# link all configuration files
# setup git
# setup pip
