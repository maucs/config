export ARCH_MIRROR='http://download.nus.edu.sg/mirror/arch/$repo/os/$arch'
export ARCH_CHROOT="arch-chroot /mnt bash"

# Preparing storage drive
sgdisk /dev/sda -o # delete all partitions
sgdisk /dev/sda -n 1:: # make new partition
sgdisk /dev/sda -A 1:set:2 # set bootable partition
mkfs.ext4 /dev/sda1

mount /dev/sda1 /mnt

echo Server = $ARCH_MIRROR > /etc/pacman.d/mirrorlist

pacstrap /mnt base syslinux gptfdisk
genfstab -p /mnt >> /mnt/etc/fstab

# copy entire folder to the new system to bootstrap
cp -r /root/*-mau-config-* /mnt/mau-config

# start woking inside chroot
$ARCH_CHROOT /mau-config/scripts/setup_chroot.sh

echo "After restarting, cd to /mau-config/scripts and run ./initial-setup.sh"
