echo en_US.UTF-8 UTF-8 >> /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
ln -s /usr/share/zoneinfo/Asia/Kuching /etc/localtime
hwclock --systohc --utc
echo "archlinux-workstation" > /etc/hostname # not working?
systemctl enable dhcpcd@enp0s3
syslinux-install_update -i -a -m
sed -i s/sda3/sda1/g /boot/syslinux/syslinux.cfg
passwd
