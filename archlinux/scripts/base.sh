# base programs and settings
export APPS='openssh zsh glances atool tree adobe-source-code-pro-fonts mlocate'

pacman -Syu --needed --noconfirm $APPS

# update locate database
updatedb
