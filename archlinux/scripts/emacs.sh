export EMACS_CONF_DIR=`cd ../../HOME/.emacs.d && pwd`

pacman -Syu --needed --noconfirm emacs

mkdir -p ~/.emacs.d
# link each files in the configuration folder, but not the folder itself
find $EMACS_CONF_DIR -type f -exec ln -fs {} ~/.emacs.d \;
