# set up ssh keys
systemctl start sshd.service
su -c "mkdir -p ~/.ssh" -s /bin/sh mau

# scp the public and private file using
# scp id_rs* mau@192.xxx.x.x:~/.ssh/
# if offending host error occurs, ssh-keygen -R hostname

su -c "chmod 600 ~/.ssh/id_rsa" -s /bin/sh mau
su -c "chmod 644 ~/.ssh/id_rsa.pub" -s /bin/sh mau
