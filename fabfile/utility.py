"""
Utilities for Fabric usage
"""


def list_packages_from_file(package_file):
    "Convert a bunch of packages from a file into a string of packages"
    with open(package_file, 'r') as f:
        package_string = f.read()
    # ugly, and contains extra spaces due to empty lines, but it works
    return ' '.join(package_string.splitlines())
