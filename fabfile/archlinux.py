"""
Not developed for the time being
"""

from fabric.api import *  # noqa
from utility import list_packages_from_file

"""
From A to Z
install(), post_installation(), install_ssh(), setup_user()
"""

arch_mirror = "http://mirror.nus.edu.sg/archlinux/$repo/os/$arch"

def pacman_install(command, sudoit=False):
    "Runs pacman -Syu --needed --noconfirm"
    run_this = "pacman -Syu --needed --noconfirm {}".format(command)
    if sudoit:
        sudo(run_this)
    else:
        run(run_this)

@task()
def install():
    "A simple script that automates installation"
    ## Remote installation
    # To install remotely, run these commands on the system
    # $ systemctl start sshd; passwd

    ## Revert bad installation if errors occured
    with settings(warn_only=True):
        run("umount /mnt")

    ## Change the language

    ## Establish an internet connection

    ## Preparing the storage drive
    # Setting bootable partition is an additional step
    run('sgdisk /dev/sda -o')  # delete all partition
    run('sgdisk /dev/sda -n 1::')  # make new partition
    run('sgdisk /dev/sda -A 1:set:2')  # set bootable partition
    run("mkfs.ext4 /dev/sda1")

    ## Mount the partitions
    run("mount /dev/sda1 /mnt")

    ## Select a mirror
    #put("conf-root/archlinux/etc/pacman.d/mirrorlist", "/etc/pacman.d/mirrorlist")
    run('echo \'Server = {}\' > /etc/pacman.d/mirrorlist'.format(arch_mirror))

    ## Install the base system
    # added extra stuffs for easier management
    run("pacstrap /mnt base base-devel openssh syslinux gptfdisk")

    ## Generate an fstab
    run("genfstab -p /mnt >> /mnt/etc/fstab")

    ## chroot and configure the base system
    install_chroot()

    # Not part of the guide
    install_extra()

    ## Unmount the partitions and reboot
    print("Run the next installation to proceed")
    run('umount /mnt; reboot')

def run_c(command):
    "Command to help managing chroot easier"
    run('arch-chroot /mnt {}'.format(command))


def install_chroot():
    "Handles installation in chroot"
    # Locale
    run_c('echo en_US.UTF-8 UTF-8 >> /etc/locale.gen')
    run_c("locale-gen")
    run_c('echo LANG=en_US.UTF-8 > /etc/locale.conf')

    # Console font and keymap

    # Time zone
    run_c('ln -s /usr/share/zoneinfo/Asia/Kuching /etc/localtime')

    # Hardware clock
    run_c('hwclock --systohc --utc')

    # Kernel modules

    # Hostname
    run_c('echo "archlinux-workstation" > /etc/hostname')

    # Configure the network
    run_c('systemctl enable dhcpcd@eth0')

    # Create an initial ramdisk environment

    # Set the root password
    run_c("passwd")

    # Install and configure a bootloader
    # syslinux requires gptfdisk for sgdisk which is used to set GPT-specific
    # boot flag
    run_c("syslinux-install_update -i -a -m")
    run_c('sed -i s/sda3/sda1/g /boot/syslinux/syslinux.cfg')


def install_extra():
    "Additional steps after installation"
    run_c('echo "[multilib]\nInclude = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf')
    run_c('systemctl enable sshd.socket')


@task
def post_installation(virtual='vbox'):
    "Run after installation is finished. Root is still assumed"
    pacman_install("xorg-server xorg-xinit xorg-server-utils mesa")
    #install_aur_helper()
    # set wheel group for sudo access
    run('echo "%wheel ALL=(ALL) ALL" > /etc/sudoers.d/g-wheel')
    # reduce syslinux boot timeout to 1.5s
    run('sed -i s/"TIMEOUT 50"/"TIMEOUT 15"/g /boot/syslinux/syslinux.cfg')
    if virtual == 'vbox':
        # set up conf for virtualbox
        pacman_install("virtualbox-guest-utils")
        run('echo vboxguest > /etc/modules-load.d/virtualbox.conf')
        run('echo vboxsf >> /etc/modules-load.d/virtualbox.conf')
        run('echo vboxvideo >> /etc/modules-load.d/virtualbox.conf')
        run('systemctl enable vboxservice')
    run('useradd -m -g wheel mau; passwd mau')
    print("Please make sure SSH key is working before proceeding to the next one.")
    run('reboot')


@task
def setup_user():
    "Setup user specific configurations. Needs some hand holding"
    # install_aur_helper() # install pacaur
    # # install official repo apps
    # app_list = list_packages_from_file('program_list/arch.pacman')
    # pacman_install(app_list, sudoit=True)
    # # install aur apps
    # app_list = list_packages_from_file('program_list/arch.aur')
    # run('pacaur --noconfirm --noedit -y {}'.format(app_list))

    run('chsh -s /usr/bin/zsh')
    run('git clone git@bitbucket.org:mau5/mau-config.git')
    with cd('mau-config'):
        run('fab all.setup')


@task
def install_ssh():
    run("mkdir -p ~/.ssh")
    put("~/.ssh/id_rsa", "~/.ssh/")
    put("~/.ssh/id_rsa.pub", "~/.ssh/")
    run("chown mau ~/.ssh/id_rsa*")
    run("sudo chown 600 ~/.ssh/id_rsa")
    run("sudo chown 644 ~/.ssh/id_rsa.pub")

def install_aur_helper():
    "Install AUR Helpers"
    run('mkdir -p aurbuild')
    with cd("aurbuild"):
        run("bash <(curl aur.sh) -si cower pacaur")
    run('rm -rf aurbuild')
