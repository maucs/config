"""
Private configurations. Require authentication.
"""

from fabric.api import *  # noqa

bitbucket_ssh = "ssh://hg@bitbucket.org/mau5"
public_repos = ['pchallenge']
repos = public_repos + ['server', 'notebook']


@task
def clone():
    """Clone public/private repos. Requires SSH keys"""
    local('mkdir -p repos')
    with lcd('repos'):
        with settings(warn_only=True):
            [local('hg clone {}/{}'.format(bitbucket_ssh, repo)) for repo in repos]
