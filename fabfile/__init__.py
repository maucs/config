"""
My config's fabfile.
Use this to ease some desktop deployment pains.

Currently tested on Ubuntu only.

Note: Run fabric only at the root level. Paths are hardcoded.
Note: SSH client AND server is required, even for local usage
"""

from fabric.api import *  # noqa

import debian  # noqa
import all  # noqa
import private  # noqa
import archlinux  # noqa


@task
def apps():
    """Installs applications listed at 'program_list' folder"""
    local('sudo aptitude install $(<program_list/program.cli.list) -y')
    local('pip install -r program_list/pip.list')


@task
def emacs_install():
    """Install Emacs 24"""
    local('sudo apt-add-repository ppa:cassou/emacs')  # add custom repository
    local('sudo aptitude update')
    local('sudo aptitude install emacs-snapshot -y')
