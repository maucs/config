from fabric.api import *  # noqa


@task
def bootstrap():
    "Initial Debian Sid installation."
    # automate so that everything in conf-root/debian-sid is
    # thrown automatically
    sudo('aptitude update && aptitude upgrade -y')
    copy_root_config()
    install_apps()
    # sed -i writes to same file, but only for gnu sed
    # . means any character, similar to *
    # * means repeat last character, it's a sed thing
    # sets bootloader timeout to 1 second, then updates it
    sudo('sed -i s/^GRUB_TIMEOUT=.*$/GRUB_TIMEOUT=1/ /boot/etc/default')
    sudo('update-grub')
    reboot()


def install_apps():
    """Install apps stated in the program list, exclusive for debian"""
    list_app = []
    from os import listdir
    # use data located in program_list, look for debian specific
    for files in listdir('program_list'):
        if files.startswith('debian'):
            with open('program_list/{}'.format(files)) as a:
                for lines in a:
                    list_app.append(lines.strip())
    sudo('aptitude install {} -y'.format(' '.join(list_app)))


@task
def copy_root_config():
    "Copy distro-wide configurations."
    with lcd('conf-root/debian-sid'):
        put('etc/apt/sources.list', '/etc/apt/sources.list', use_sudo=True)
        put('etc/apt/apt.conf', '/etc/apt/apt.conf', use_sudo=True)


@task
def user_configuration():
    "User configuration, not related to root or anything"
    local('chsh -s `which zsh`')
