"""
Not developed for the time being
"""

from fabric.api import *  # noqa

packagesite = "http://pkgbeta.freebsd.org/freebsd-9-amd64/2012-07-04/"


@task
def bootstrap():
    """Bootstrap FreeBSD 9. Use pkgng instead. Run it as root. Do it once only"""
    # use pkgng from portsnap
    with settings(shell="/bin/sh -c"):
        run("portsnap fetch extract")
        # installs pkg2ng
        with cd('/usr/ports/ports-mgmt/pkg'):
            run('make install clean')
        run('echo PACKAGESITE: {} > /usr/local/etc/pkg.conf'.format(packagesite))  # add new repo site
        run('/usr/local/sbin/pkg2ng')  # update db package
        run('pw user mod mau -G wheel')  # add user to wheel group, assuming username is mau
        # when edit sudo, set wheel as user who gets to use sudo
