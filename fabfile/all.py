"""
Mainly platform agnostic.

Optimized for Linux, may run in BSD, no guarantee on Windows
"""

from fabric.api import *
from os import getcwd
from re import escape

pwd = getcwd()  # assumes config folder is root


def print_manual_work():
    "Print instructions that is needed to run manually for now"
    print("Use quicklisp to install SLIME, and run (ql:add-to-init-file)")
    print("Setup weechat, follow readme.rst")
    print("Remember to set up git name and email address")
    print("Run 'fbsetbg ~/.wallpaper' to set wallpaper. Do it once only")


@task
def create_and_link():
    """Creates directories that is required, and links configuration files
    Read man for each function that you never heard of."""
    conf_path = "{}/HOME".format(pwd)  # get config path
    esc_conf_path = escape(conf_path) + '\/'  # config's path, with /'s escaped
    with lcd("~/"):
        # skeleton command, using find, piped to sed, and then use each
        # arguments using xargs find and xargs argument depends on what is
        # entered in sed always remove the config path. result is what you
        # would get when you list a home directory
        cmd = "find {} -type {{}} | sed 's/{}//g' | xargs {{}}".format(conf_path, esc_conf_path)
        # list all directories first, then list all files
        local(cmd.format('d', '-n 1 echo'))
        local(cmd.format('f', '-n 1 echo'))
        # create directories based on layout of the config path
        local(cmd.format('d', 'mkdir -p'))
        # link files base on layout of config path
        local(cmd.format('f', '-I f0f ln -fs {}/f0f f0f'.format(conf_path)))
        # if ~/.zsh/antigen.zsh exists, do nothing. else download remotely
        local('[ -f ".zsh/antigen.zsh" ] || curl https://raw.github.com/zsh-users/antigen/master/antigen.zsh > .zsh/antigen.zsh')
        local('[ -f ".wallpaper" ] || curl -o .wallpaper http://i.imgur.com/JXCcWDI.jpg')


@task
def pip(pip_list="program_list/pip"):
    "Installs python application using pip"
    with settings(warn_only=True):
        local('pip install -r {}'.format(pip_list))


@task
def git():
    "Set up git related configurations"
    def config(command):
        return local("git config --global {}".format(command))
    #config('user.name "Your Name Here"')
    #config('user.email "your-email@mail.com"')
    config("core.excludesfile ~/.gitignore-global")
    config("color.ui auto")
    config("push.default simple")
    config('alias.lg "log --graph --decorate --oneline"')


@task
def setup():
    "Setup user configuration and environments"
    local('virtualenv ~/.python')
    create_and_link()
    pip()
    git()
    print_manual_work()
